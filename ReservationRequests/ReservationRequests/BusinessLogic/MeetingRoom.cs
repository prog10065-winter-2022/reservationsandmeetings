﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace ReservationRequests.BusinessLogic
{
    public enum RoomLayoutType
    {
        HollowSquare,
        UShape,
        Classroom,
        Auditorium,
    }

    public class MeetingRoom
    {
        //ObservableCollection of ReservationRequests
        ObservableCollection<ReservationRequest> _requests = new ObservableCollection<ReservationRequest>();

        public ObservableCollection<ReservationRequest> Requests => _requests; 

        string _roomNumber;
        int _seatingCapacity;
        RoomLayoutType _layoutType;
        string _imageFileName;

        /// <summary>
        /// Constructor for MeetingRoom class
        /// </summary>
        /// <param name="roomNumber">The room number of the selected room</param>
        /// <param name="seatingCapacity">The seating capacity of the selected room</param>
        /// <param name="layoutType">The layout of the selected room</param>
        /// <param name="imageFileName">The name of the image file for the room</param>
        public MeetingRoom(string roomNumber, int seatingCapacity, RoomLayoutType layoutType, string imageFileName)
        {
            RoomNumber = roomNumber;
            SeatingCapacity = seatingCapacity;
            LayoutType = layoutType;
            ImageFileName = imageFileName;

        }

        public RoomLayoutType LayoutType
        {
            get { return _layoutType; }
            private set { _layoutType = value; }
        }

        public string RoomNumber
        {
            get { return _roomNumber; }
            private set
            {
                //the RoomNumber cannot be empty
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Room Number Must be Included");
                _roomNumber = value;
            }
        }
        public int SeatingCapacity
        {
            get { return _seatingCapacity; }
            private set
            {
                //SeatingCapacity must be greater than 0
                if (value < 0)
                    throw new Exception("Seating Capacity Must Be Greater than 0");
                _seatingCapacity = value;
            }
        }

        public string ImageFileName
        {
            get { return _imageFileName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new Exception("Image File Name Must Be Included");

                _imageFileName = value;
            }
        }

        public string RoomTypeIcon => $"{ImageFileName}.png";


        /// <summary>
        /// Adds the reservation request to the observable collection for the selected room
        /// </summary>
        /// <param name="reservationRequest">The reservation which is created in the AddRequestPage</param>
        /// <returns></returns>
        public ObservableCollection<ReservationRequest> AddReservationRequest(ReservationRequest reservationRequest)
        {
            _requests.Add(reservationRequest);

            return _requests;
        }

    }
}
