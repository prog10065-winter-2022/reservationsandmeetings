﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReservationRequests.BusinessLogic
{
    enum RequestStatus
    {
        Accepted,
        Rejected,
        Pending,
    }


    public class ReservationRequest
    {
        private readonly Random random = new Random();
        int _requestId;
        string _requestedBy;
        string _description;
        DateTime _startDate;
        TimeSpan _startTime;
        DateTime _endDate;
        TimeSpan _endTime;
        int _participationCount;
        RequestStatus _status = RequestStatus.Pending;

        /// <summary>
        /// The constructor for ReservationRequest
        /// </summary>
        /// <param name="requestedBy">The name of the person who requested</param>
        /// <param name="description">Short description of the meeting</param>
        /// <param name="startDate">The start date of the meeting</param>
        /// <param name="startTime">The time the meeting will start</param>
        /// <param name="endTime">The time the meeting will end</param>
        /// <param name="participationCount">The number of participants</param>
        public ReservationRequest(string requestedBy, string description, DateTime startDate, TimeSpan startTime, TimeSpan endTime, int participationCount)
        {
            _requestId = random.Next(1, 999);
            _requestedBy = requestedBy;
            _description = description;
            _startDate = startDate;
            _startTime = startTime;
            _endDate = startDate;
            _endTime = endTime;
            ParticipationCount = participationCount;
        }

      public int ParticipationCount
        {
            get { return _participationCount; }
            set
            {
                if (_participationCount < 0)
                    throw new Exception("At Least One Participant Required");
            }
        }

        //override the ToString method
        public override string ToString()
        {
            return $"Requested by {_requestedBy} for {_description} on {_startDate} from {_startTime} to {_endTime}, Status: {_status}";
        }

    }
}
