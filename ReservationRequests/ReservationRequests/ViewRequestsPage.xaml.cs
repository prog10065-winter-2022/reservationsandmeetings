﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ReservationRequests.BusinessLogic;
using System.Collections.ObjectModel;

namespace ReservationRequests
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ViewRequestsPage : ContentPage
    {
        MeetingRoom _selectedRoom;

        /// <summary>
        /// The constructor for the view requests page
        /// </summary>
        /// <param name="selectedRoom"></param>
        public ViewRequestsPage(MeetingRoom selectedRoom)
        {
            
            InitializeComponent();

            _selectedRoom = selectedRoom;
         
            //populate the listview with the reservations list from the meeting room

            LstRoomReservations.ItemsSource = selectedRoom.Requests;

            this.BindingContext = selectedRoom;
        }

        public MeetingRoom SelectedRoom => _selectedRoom;

        /// <summary>
        /// When the button is clicked the user goes back to the PickARoomPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBackToRooms(object sender, EventArgs e)
        {
            Navigation.PopToRootAsync();
        }

        
    }
}