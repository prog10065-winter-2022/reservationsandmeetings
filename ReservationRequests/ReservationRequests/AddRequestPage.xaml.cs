﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ReservationRequests.BusinessLogic;
namespace ReservationRequests
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddRequestPage : ContentPage
    {
        MeetingRoom _selectedRoom;
        /// <summary>
        /// the constructor for the AddRequestPage
        /// </summary>
        /// <param name="selectedRoom">The Selected Room from the PickRoomPage</param>
        public AddRequestPage(MeetingRoom selectedRoom)
        {
            InitializeComponent();

            _selectedRoom = selectedRoom;

            this.BindingContext = _selectedRoom;
        }

        public MeetingRoom SelectedRoom => _selectedRoom;

        /// <summary>
        /// Creates an object of ReservationRequest from the input by the user
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        private ReservationRequest CaptureReservationInfo()
        {

            ReservationRequest request;

            string requestedBy = TxtUserName.Text;
            string description = TxtDescription.Text;
            DateTime startDate = PicStartDate.Date;
            TimeSpan startTime = PicStartTime.Time;
            TimeSpan endTime = PicEndTime.Time;                                         
            int participationCount = int.Parse(TxtParticipantCount.Text);
            if (participationCount > SelectedRoom.SeatingCapacity)
            {
                throw new Exception("Participants cannot be greater than Seating Capacity");
            }
       
            request = new ReservationRequest(requestedBy, description, startDate, startTime, endTime, participationCount);

            return request;
            
        }

        /// <summary>
        /// Captures the reservation request in a variable and then add that request to the meeting room
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAddRequest(object sender, EventArgs e)
        {
            //do try and except for if the number of participants can be accomodated
            ReservationRequest request = CaptureReservationInfo();


            SelectedRoom.AddReservationRequest(request);

        }

        /// <summary>
        /// When the button is clicked it moves back to the PickRoomPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnBackToRooms(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}