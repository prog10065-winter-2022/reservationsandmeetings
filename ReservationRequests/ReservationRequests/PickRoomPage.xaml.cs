﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ReservationRequests.BusinessLogic;

namespace ReservationRequests
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PickRoomPage : ContentPage
    {
        MeetingRoom _selectedRoom;
        public MeetingRoom SelectedRoom
        {
            get { return _selectedRoom; }
            set
            {
                _selectedRoom = value;
                OnPropertyChanged("SelectedRoom");
            }
        }
        /// <summary>
        /// Represents the PickRoomPage
        /// </summary>
        public PickRoomPage()
        {
            InitializeComponent();

            //hard coded values for the rooms
            MeetingRoom hollowSquare = new MeetingRoom("Room 102", 20, RoomLayoutType.HollowSquare, "HollowSquare.png");
            MeetingRoom uShape = new MeetingRoom("Room 103", 20, RoomLayoutType.UShape, "UShape.png");
            MeetingRoom classroom = new MeetingRoom("Room 202", 40, RoomLayoutType.Classroom, "Classroom.png");
            MeetingRoom auditorium = new MeetingRoom("Room 105", 200, RoomLayoutType.Auditorium, "Auditorium.png");

            //make a list of these meeting rooms
            List<MeetingRoom> meetingRooms = new List<MeetingRoom>() { hollowSquare, uShape, classroom, auditorium };

            //initialize the listview with these rooms
            LstMeetingRooms.ItemsSource = meetingRooms;

            //set the binding context
            BindingContext = this;

        }
        /// <summary>
        /// When the button is clicked it moves to the AddRequestPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void AddRequest(object sender, EventArgs e)
        {
            //if no item is selected it displays an alert
            if (LstMeetingRooms.SelectedItem == null)
                await DisplayAlert("Cannot Add Your Request", "Please select a room to add first", "Ok");

            //moves to the AddRequestPage and takes the SelectedRoom as parameter
            else
            await Navigation.PushAsync(new AddRequestPage(SelectedRoom));
        }

        /// <summary>
        /// When the button is clicked it moves to the ViewRequestsPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void ViewRequests(object sender, EventArgs e)
        {
            //if no item is selected it displays an alert
            if (LstMeetingRooms.SelectedItem == null)
               await DisplayAlert("Cannot View Your Request", "Please select a room to view first", "Ok");
            //moves to the ViewRequestsPage and takes the SelectedRoom as parameter
            else
                await Navigation.PushAsync(new ViewRequestsPage(SelectedRoom));
            
        }
    }
}